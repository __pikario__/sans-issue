extends Control

enum ETAT{
	rien,
	deplacement,
	afficheur,
	trampoline_ajouter,
	trampoline_retirer
}

var deja_afficheur = false
var deja_poser_trampoline = false
var deja_supprimer_trampoline = false

var etat = ETAT.rien

# c'est de la narration, mais bon c'est juste ça, ça passe
var deja_mail = [false, false, false]

###################
# * lorsqu'on démarre une nouvelle partie après avoir déjà joué
###################
func reinitialisation():
	deja_afficheur = false
	deja_poser_trampoline = false
	deja_supprimer_trampoline = false
	etat = ETAT.rien
	deja_mail = [false, false, false]

###################
# *
###################
func afficher(id_texte):
	$CanvasLayer/Panel/Label.set_text(tr(id_texte))
	$Apparition.interpolate_property($CanvasLayer/Panel, "modulate", Color(1,1,1,0), Color(1,1,1,1), 1.00, 1, Tween.EASE_IN, 0)
	$Apparition.start()

###################
# *
###################
func disparaitre():
	$Disparition.interpolate_property($CanvasLayer/Panel, "modulate", Color(1,1,1,1), Color(1,1,1,0), 1.00, 1, Tween.EASE_IN, 0)
	$Disparition.start()

###################
# *
###################	
func afficher_deplacement():
	afficher("TUTO-DEPLACEMENT")
	etat = ETAT.deplacement

###################
# *
###################
func afficher_afficheur():
	if not deja_afficheur:
		afficher("TUTO-AFFICHEUR")
		etat = ETAT.afficheur

###################
# *
###################
func afficher_poser_trampoline():
	if not deja_poser_trampoline:
		afficher("TUTO-TRAMPOLINE-POSER")
		etat = ETAT.trampoline_ajouter

###################
# *
###################
func _on_TimerSuppression_timeout():
	if not deja_supprimer_trampoline:
		afficher("TUTO-TRAMPOLINE-RETIRER")
		etat = ETAT.trampoline_retirer
	
###################
# *
###################
func _process(_delta):
	match etat:
		ETAT.deplacement:
			if Input.is_action_just_pressed("ui_right") or \
			Input.is_action_just_pressed("ui_left") or \
			Input.is_action_just_pressed("ui_up") or \
			Input.is_action_just_pressed("ui_down"):
				disparaitre()
				etat = ETAT.rien
		ETAT.afficheur:
			if Input.is_action_just_pressed("espace"):
				disparaitre()
				etat = ETAT.rien
				deja_afficheur = true
		ETAT.trampoline_ajouter:
			if Input.is_mouse_button_pressed(BUTTON_LEFT):
				disparaitre()
				etat = ETAT.rien
				deja_poser_trampoline = true
				$TimerSuppression.start()
		ETAT.trampoline_retirer:
			if Input.is_mouse_button_pressed(BUTTON_LEFT):
				disparaitre()
				etat = ETAT.rien
				deja_supprimer_trampoline = true

