extends Node

var POINTEURS = [load("res://ressources/pouvoirs/pointeur/pointeur00.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur01.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur02.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur03.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur04.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur05.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur06.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur07.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur08.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur09.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur10.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur11.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur12.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur13.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur14.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur15.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur16.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur17.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur18.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur19.png"),
				load("res://ressources/pouvoirs/pointeur/pointeur20.png"),
				]

var NORMAL = load("res://ressources/pouvoirs/pointeur/pointeur00.png")
var PAS_DE_CURSEUR = load("res://ressources/pouvoirs/pointeur/pointeur_vide.png")

var EPUISE = load("res://ressources/pouvoirs/pointeur/pointeur21.png")

var index = 0
var chrono = 0

enum ETAT{
	normal,
	damier_trampoline_dispo,
	damier_trampoline_epuise,
	absent
}
var etat = ETAT.normal

###################
# *
###################
func _ready():
	normal()

###################
# *
###################
func _process(delta):
	if ETAT.damier_trampoline_dispo == self.etat:
		chrono += delta
		
		if chrono >0.02:
			Input.set_custom_mouse_cursor(POINTEURS[index])
			index = (index+1) %POINTEURS.size()
			chrono = 0
			
###################
# *
###################
func normal():
	self.etat = ETAT.normal
	Input.set_custom_mouse_cursor(NORMAL)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

###################
# *
###################
func trampoline_dispo():
	self.etat = ETAT.damier_trampoline_dispo
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

###################
# *
###################
func trampoline_epuise():
	self.etat = ETAT.damier_trampoline_epuise
	Input.set_custom_mouse_cursor(EPUISE)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

###################
# *
###################
func pas_de_curseur():
	self.etat = ETAT.absent
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

###################
# *
###################
func changer_etat(etat):
	match etat:
		ETAT.normal : normal()
		ETAT.damier_trampoline_dispo : trampoline_dispo()
		ETAT.damier_trampoline_epuise : trampoline_epuise()
		ETAT.absent : pas_de_curseur()
