extends Control

signal ecran_titre
signal pause_change_etat

onready var souris = get_node("/root/Souris")
var cache_etat_souris

var desactive = false

###################
# *
###################
func _ready():
	self.hide()
	$Panel/Options/PleinEcran.pressed = OS.window_fullscreen

###################
# *
###################
func _process(_delta):
	if not desactive and Input.is_action_just_pressed("pause"):
		if not self.visible:
			cache_etat_souris = souris.etat
			souris.normal()
		else:
			souris.changer_etat(cache_etat_souris)
		
		get_tree().paused = not get_tree().paused
		self.visible = not self.visible
		emit_signal("pause_change_etat", self.visible)

###################
# *
###################
func desactiver(booleen):
	if booleen == true:
		get_tree().paused = false
		self.visible = false
	desactive = booleen
		
###################
# *
###################
func sourdine(booleen):
	for i in range(AudioServer.get_bus_count()):
		AudioServer.set_bus_mute(i, booleen)

###################
# *
###################
func _on_CheckButton_pressed():
	sourdine(not $Panel/Options/Son.pressed)

###################
# *
###################
func _on_Quitter_pressed():
	$Popup.popup()

###################
# *
###################
func _on_Quitter2_pressed():
	$Popup.hide()
	desactiver(true)
	emit_signal("ecran_titre")

###################
# *
###################
func _on_Annuler_pressed():
	$Popup.hide()

###################
# *
###################
func _on_PleinEcran_pressed():
	OS.window_fullscreen = $Panel/Options/PleinEcran.pressed
