extends "res://generiques/Sauteur.gd"

var pousse = false
var chrono = 0

###################
# *
###################
func apparition():
	$Specifique.play("tomber")

###################
# *
###################
func _on_AnimationPlayer_animation_started(_anim_name):
	$Specifique.play("normal")

###################
# *
###################
func pousser():
	if true != pousse:
		$Pousse_Debut.play()
		$Pousse_Debut.connect("finished",$Pousse_Milieu,"play",[],CONNECT_ONESHOT)
		pousse = true
		self.viens_de_sauter = false
	chrono = 0
	
###################
# *
###################
func _process(delta):
	if pousse:
		chrono += delta
		if chrono > 0.02:
			pousse = false
			$Pousse_Debut.stop()
			if $Pousse_Debut.is_connected("finished",$Pousse_Milieu,"play"):
				$Pousse_Debut.disconnect("finished",$Pousse_Milieu,"play")
			$Pousse_Milieu.stop()
			# plus agréable sans le son de fin
			#$Pousse_Fin.play()

