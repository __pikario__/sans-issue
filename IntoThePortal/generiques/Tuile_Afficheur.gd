extends StaticBody2D

var AFFICHEUR = load("res://generiques/Afficheur.tscn")

signal debut_affichage
signal fin_affichage

var afficheur
var messages = []

var casse = false

func _ready():
	$Sprite.frame = 0

func ouverture():
	$AnimationPlayer.play('ouverture')
	$AnimationPlayer.connect("animation_finished",self,"_on_AnimationPlayer_animation_finished",[],CONNECT_ONESHOT)

func fermeture():
	$AnimationPlayer.play_backwards('ouverture')
	afficheur.queue_free()
	emit_signal("fin_affichage")

func _on_AnimationPlayer_animation_finished(_anim_name):
	afficheur = AFFICHEUR.instance()
	afficheur.connect("fin",self,"fermeture")
	self.add_child(afficheur)
	afficheur.messages = self.messages
	if self.casse:
		afficheur.casse()
	emit_signal("debut_affichage")

#########
# *
#########
func casse():
	self.casse = true
