extends Node2D

export(Texture) var image = load("res://ressources/acte3/affiche_bonheur.png")

###################
# *
###################
func _ready():
	$Sprite.texture = image

###################
# *
###################
func _on_Allumage_body_entered(body):
	if body.is_in_group('personnage') and not $Light2D.visible:
		$Light2D.visible = true
		$AnimationPlayer.play('variations')
		$AudioStreamPlayer2D.play()
