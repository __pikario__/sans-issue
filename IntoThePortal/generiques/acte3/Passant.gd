extends "res://generiques/Sauteur.gd"

signal interaction_possible
signal interaction_impossible
signal interaction

var animation_posture_courante = "debout"
var animation_direction_courante = "arriere"

var marcher = false

var points = []
var index_chemin = 0

#########
# *
#########
func _ready():
	$Label.text = self.name
	
#########
# *
#########		
func desactiver(booleen):
	self.set_process(booleen)
	$Sprite/AnimationPlayer.play("debout_"+animation_direction_courante)
	
#########
# *
#########	
func _physics_process(delta):
	if marcher:
		var direction = Vector2(0,0)
		if index_chemin < points.size() - 1 :
			var point = points[index_chemin]
			
			if self.global_position.distance_to(point) < 10:
				index_chemin += 1
				point = points[index_chemin]

			var deplacement = (point-self.global_position).normalized() * 50
			self.move_and_slide(deplacement)
			direction = deplacement.normalized().round()
			
			orienter(direction)

#########
# *
#########	
func definir_courbe_et_transform(courbe,transformation):
	var points_courbe = courbe.get_baked_points()
	for point in points_courbe:
		points.append(transformation * point)
	
#########
# *
#########	
func orienter(direction):
	if direction == Vector2(0,1):
		animation_posture_courante = "marche"
		animation_direction_courante = "avant"
	elif direction == Vector2(0,-1):
		animation_posture_courante = "marche"
		animation_direction_courante = "arriere"
	elif direction == Vector2(1,0):
		animation_posture_courante = "marche"
		animation_direction_courante = "gauche"
	elif direction == Vector2(-1,0):
		animation_posture_courante = "marche"
		animation_direction_courante = "gauche"
	else:
		animation_posture_courante = "debout"
		
	# animation
	var animation = animation_posture_courante+"_"+animation_direction_courante
	if(animation != $Sprite/AnimationPlayer.current_animation):
		$Sprite/AnimationPlayer.play(animation_posture_courante+"_"+animation_direction_courante)
