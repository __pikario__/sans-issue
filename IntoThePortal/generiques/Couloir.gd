extends Node2D

var TUILE = load("res://generiques/Tuile.tscn")
var JOUEUR = load("res://generiques/Personnage.tscn")
var PHRASE = load("res://generiques/Phrase.tscn")
var LETTRE = load("res://generiques/Lettre.tscn")

var taille_tuile

var index = 0
var joueur 
var declencheur

###################
# *
###################
func _ready():
	self.taille_tuile = TUILE.instance().taille()
	
	self.declencheur = Area2D.new()
	self.declencheur.connect("body_entered",self,"avance")
	
	poser_ligne()
	poser_ligne()
	poser_ligne()
	
	self.joueur = JOUEUR.instance()
	self.joueur.z_index = 1
	self.joueur.vitesse = 2
	
	self.add_child(joueur)
	self.joueur.global_position = Vector2(-0.5,0) * TUILE.instance().taille()
	
	var camera = Camera2D.new()
	camera.current = true
	camera.offset = Vector2(0,-4) *TUILE.instance().taille()
	camera.drag_margin_top = 0.1
	camera.drag_margin_bottom = 0
	self.joueur.add_child(camera)

###################
# *
###################
func poser_ligne():
	var tuile_gauche = TUILE.instance()
	var tuile_droite = TUILE.instance()
	
	tuile_gauche.position = Vector2(-1,index) * tuile_gauche.taille()
	tuile_droite.position = Vector2(0,index) * tuile_droite.taille()

	$Declencheur.position = Vector2(0,index+2) * tuile_droite.taille()
	
	tuile_gauche.apparition(true)
	tuile_droite.apparition(true)

	self.add_child(tuile_gauche)
	self.add_child(tuile_droite)
	
	index -= 1
	if index %10 == 0:
		poser_phrase()

###################
# *
###################
func poser_phrase():
	
	var lettres = []
	
	for x in range(-5,5):
		for y in range(2):
			var tuile = TUILE.instance()
			tuile.position = Vector2(x, index - y) *  tuile.taille()
			self.add_child(tuile)
			tuile.apparition(true)
			
			var premiere_ligne = []
			var seconde_ligne = []
			for i in range(-1,1):
				for j in range(-1,1):
					var lettre = LETTRE.instance()
					tuile.add_child(lettre)
					lettre.position = Vector2(i,j) * 28 + Vector2(14,14)
					if j == -1:
						premiere_ligne.append(lettre)
					else:
						seconde_ligne.append(lettre)
			lettres += premiere_ligne
			lettres += seconde_ligne
	
	var phrase = "Bonjour"
	var index = 0
	for lettre in lettres:
		if index < phrase.length():
			lettre.lettre(phrase[index])
			index+=1
		else:
			lettre.lettre("")
					
#	var noeud_texte = Node2D.new()
#	var texte = Label.new()
#	texte.text = "Bonjour"
#	print(texte.theme)
#	noeud_texte.add_child(texte)
#	noeud_texte.position = Vector2(0,index) * taille_tuile
#	self.add_child(noeud_texte)	
			
	index -= 2

###################
# *
###################
func _on_Declencheur_body_entered(body):
	if body == joueur:
		poser_ligne()
		poser_ligne()
		poser_ligne()
