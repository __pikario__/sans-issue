extends KinematicBody2D

signal tomber

var invincible = false
var direction_saut = Vector2(0,0)
var viens_de_sauter = false

var lecteur_perso

func _ready():
	# on duplique l'animation de saut car celle-ci est partagée entre instances
	# sinon les sauteurs qui sautent en même temps se parasitent
	lecteur_perso = $AnimationPlayer
	lecteur_perso.add_animation("saut",$AnimationPlayer.get_animation("saut").duplicate())

func sauter(origine,destination):
	var anim = lecteur_perso.get_animation("saut")
	
	# la fonction manipule des coordonnées globales
	# pour l'animation on repasse en local
	anim.track_set_key_value(0,0,self.get_parent().global_transform.xform_inv(origine))
	anim.track_set_key_value(0,1,self.get_parent().global_transform.xform_inv(destination))	
	
	direction_saut = (destination - origine).normalized()
	
	lecteur_perso.play("saut")
	lecteur_perso.connect("animation_finished",self,"fin_saut",[],CONNECT_ONESHOT)
	if get_node_or_null('Saut'):
		$Saut.play()
		
	viens_de_sauter = true
	
func fin_saut(_truc):
	if get_node_or_null('PoussiereArrivee'):
		$PoussiereArrivee.emitting = true	
	if get_node_or_null('Au_sol'):
		$Au_sol.play()
	
func tomber():
	if !invincible and not self.en_saut():
		if get_node_or_null('Au_sol'):
			$Au_sol.stop()
		if get_node_or_null('PoussiereArrivee'):
			$PoussiereArrivee.hide()
		lecteur_perso.play("tomber")
		lecteur_perso.connect("animation_finished",self,"emit_signal")
		# son
		if get_node_or_null("Saut") and $Saut.playing:
			$Saut.stop()
		if get_node_or_null("Tomber"):
			$Tomber.play()
	
func tomber_violement():
	if !invincible:
		lecteur_perso.play("tomber")
		lecteur_perso.connect("animation_finished",self,"emit_signal")
		if get_node_or_null("Mur"):
			$Mur.play()
			if get_node_or_null("Tomber"):
				$Mur.connect("finished",$Tomber,"play")
	
func en_saut():
	return lecteur_perso.current_animation == 'saut'
	
func _process(_delta):
	var anim = lecteur_perso.get_animation("saut")
	anim.get_class()
	pass	
