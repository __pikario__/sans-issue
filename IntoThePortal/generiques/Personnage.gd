extends "res://generiques/Sauteur.gd"

signal interaction_possible
signal interaction_impossible
signal interaction

var vitesse = 2

var animation_posture_courante = "debout"
var animation_direction_courante = "arriere"

var mouvement = Vector2(0,0);

var chrono = 0

var touches_appuyees = [false,false,false,false]

##################################
# * 
##################################
func _ready():
	self.set_process(true)
		
##################################
# * 
##################################
func _process(delta):
	if $AnimationPlayer.current_animation != "tomber":
		
		animation_posture_courante = "debout"
		
		# DEPLACEMENT
		
		var ancien_mouvement = mouvement
		mouvement = Vector2(0,0)
		
		# si bas est appuyé -> on va vers le bas, jusqu'à ce qu'une autre touche soit appuyée
		if Input.is_action_pressed("ui_down"):
			var m = Vector2(0,1)
			if (touches_appuyees[0] == false) or ancien_mouvement == m or seule_touche_appuyee(0): 
				animation_posture_courante = "marche"
				animation_direction_courante = "avant"
				$Sprite.scale.x=1
				mouvement = m
			touches_appuyees[0] = true
		else:
			touches_appuyees[0] = false
			
		# haut
		if Input.is_action_pressed("ui_up"):
			var m = Vector2(0,-1)
			if (touches_appuyees[1] == false) or (ancien_mouvement == m  and mouvement == Vector2(0,0)) or seule_touche_appuyee(1): 
				animation_posture_courante = "marche"
				animation_direction_courante = "arriere"
				$Sprite.scale.x=1
				mouvement = m
			touches_appuyees[1] = true
		else:
			touches_appuyees[1] = false
		
		# droite
		if Input.is_action_pressed("ui_right"):
			var m = Vector2(1,0)
			if (touches_appuyees[2] == false) or (ancien_mouvement == m and mouvement == Vector2(0,0)) or seule_touche_appuyee(2): 
				animation_posture_courante = "marche"
				animation_direction_courante = "gauche"
				$Sprite.scale.x=-1
				mouvement = m
			touches_appuyees[2] = true
		else:
			touches_appuyees[2] = false
		
		# gauche
		if Input.is_action_pressed("ui_left"):
			var m = Vector2(-1,0)
			if (touches_appuyees[3] == false) or (ancien_mouvement == m and mouvement == Vector2(0,0)) or seule_touche_appuyee(3): 
				animation_posture_courante = "marche"
				animation_direction_courante = "gauche"
				$Sprite.scale.x=1
				mouvement = m
			touches_appuyees[3] = true
		else:
			touches_appuyees[3] = false
			
		# #######
			
		if Input.is_action_just_pressed('triche'):
			if Input.is_action_pressed("espace"):
				invincible = !invincible
				print('invincible : ',invincible)
	
		# animation
		var animation = animation_posture_courante+"_"+animation_direction_courante
		if(animation != $Sprite/AnimationPlayer.current_animation):
			$Sprite/AnimationPlayer.playback_speed = 1.5
			$Sprite/AnimationPlayer.play(animation_posture_courante+"_"+animation_direction_courante)
		
		# mouvement
		if self.has_method('move_and_collide'):
			var collision = self.move_and_collide(mouvement * vitesse * delta * 100)
			if collision != null:
				if collision.collider.has_method("move_and_collide"):
					collision.collider.move_and_collide(mouvement * vitesse/2)
				if collision.collider.has_method("pousser"):
					collision.collider.pousser()
				if collision.collider.is_in_group("passant"):
					repousser(-mouvement)
			
##################################
# * 
##################################
func desactiver(booleen):
	self.set_process(not booleen)
	$Sprite/AnimationPlayer.play("debout_"+animation_direction_courante)
	
##################################
# * 
##################################
func reinitialiser():
	self.rotation_degrees = 0
	self.scale = Vector2(1,1)
	self.modulate = Color(1,1,1,1)
	$CollisionShape2D.disabled = false

##################################
# * 
##################################
func repousser(direction):
	self.move_and_collide(direction * 10)
	
##################################
# * 
##################################
func seule_touche_appuyee(n):
	for i in len(touches_appuyees):
		if i != n and touches_appuyees[i]:
			return false
	return true
