extends Node2D

var TRAMPOLINE = load("res://generiques/Trampoline.tscn")
var TUILE = load("res://generiques/Tuile.tscn")

var trampolines = []

var max_trampolines = 0

# sert pour éviter les erreurs d'appui à l'ajout ou à la suppression d'un trampoline
const SEUIL_TEMPS_APPUI = 0.2
var minuteur = SEUIL_TEMPS_APPUI

var taille_tuile

onready var souris = get_node("/root/Souris")
onready var tuto = get_node("/root/Tuto")

var mode_construction = false

#########
# *
#########
func _ready():
	taille_tuile = TUILE.instance().taille()
	if self.max_trampolines > 0:
		souris.trampoline_dispo()
	else:
		souris.normal()

#########
# *
#########
func _process(delta):
	minuteur+=delta;
	if minuteur > SEUIL_TEMPS_APPUI:
		set_process(false)
	
#########
# permet de placer des trampolines
#########	
func selection(tuile,clic):
	if minuteur >= SEUIL_TEMPS_APPUI:
		# vérification que la tuile ne possède pas déjà quelque chose sur elle
		var deja_occupe = false
		for enfant in tuile.get_children():
			if enfant.get_filename() == TRAMPOLINE.get_path():
				deja_occupe = true
				
		# vérification qu'il reste des trampolines à placer
		if	trampolines.size() < max_trampolines:
			if !deja_occupe:
				var trampoline = TRAMPOLINE.instance()
				trampoline.connect("saut",self,"saut")
				trampoline.connect("supprime",self,"suppression",[tuile])
				tuile.add_child(trampoline)
				trampoline.sens(clic)
				trampoline.show()
				trampolines.append(trampoline)
				tuile.connect("disparition",self,"suppression",[trampoline,tuile,true],CONNECT_ONESHOT)
				tuile.connect("enlevee",self,"suppression",[trampoline,tuile,true],CONNECT_ONESHOT)
				if trampolines.size() > max_trampolines-1:
					get_tree().call_group("tuiles","trampoline",false)
					souris.trampoline_epuise()

#########
# *
#########
func suppression(trampoline,tuile,car_tuile_disparue=false):
	if (not self.mode_construction or car_tuile_disparue) and weakref(trampoline).get_ref() != null:
		tuile.disconnect("disparition",self,"suppression")
		tuile.disconnect("enlevee",self,"suppression")
		trampolines.erase(trampoline)
		trampoline.queue_free()
		if not tuto.deja_supprimer_trampoline:
			tuto.deja_supprimer_trampoline = true
		if	trampolines.size() < max_trampolines:
			get_tree().call_group("tuiles","trampoline",true)
			minuteur = 0
			set_process(true)
			souris.trampoline_dispo()

#########
# provoque le saut d'un corps dans une direction donnnée
#########			
func saut(trampoline,sauteur,direction):
	sauteur.sauter(trampoline.global_position, trampoline.global_position + taille_tuile*2*direction)
