extends Node2D

signal active
signal desactive

var dedans = []

###################
# *
###################
func _on_Area2D_body_entered(body):
	$Sprite.frame = 1
	dedans.append(body)
	emit_signal("active")
	$Clic.play()

###################
# *
###################
func _on_Area2D_body_exited(body):
	dedans.erase(body)
	if dedans.empty():
		$Sprite.frame = 0
	emit_signal("desactive")

###################
# *
###################
func est_active():
	return !dedans.empty() 