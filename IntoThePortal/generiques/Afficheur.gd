extends Node2D

signal fin

var LETTRE = load("res://generiques/Lettre.tscn")

const TAILLE_H = 40
const TAILLE_V = 8

var lettres = []

var index_courant = 0
var message_courant = 0

var messages = ["Bonjour et bienvenue ex-citoyen, \n \n voici venue l'oportunité de reconquerir ton titre !","Te voici face à une série d'épreuves te permettant de montrer ta détermination et ta motivation pour notre projet de société."]

var phrase = ""

onready var tuto = get_node("/root/Tuto")

###################
# *
###################
func _ready():
	for y in range(-TAILLE_V/2,TAILLE_V/2):
		for x in range(-TAILLE_H/2,TAILLE_H/2):
			var lettre = LETTRE.instance()
			self.add_child(lettre)
			lettre.position = Vector2(x,y) * Vector2(16,35)
			lettres.append(lettre)

	tuto.afficher_afficheur()

###################
# *
###################
var doit_ecrire = false
func ecrire(phrase):
	$Ecriture.play()
	self.index_courant = 0		
	self.phrase = formater(tr(phrase))
	aleatoire()
	compteur_aleatoire = 0
	doit_ecrire = true
	
###################
# *
###################
func formater(phrase):
	phrase = phrase.replace("\n"," \n ")
	
	var finale = ""
	for mot in phrase.split(" "):
		if len(finale)%TAILLE_H + len(mot) >= TAILLE_H or mot == '\n':
			for _i in range(TAILLE_H - len(finale)%TAILLE_H):
				finale += " "
		if mot != '\n':
			finale += mot + " "
	return finale
	
###################
# *
###################
func aleatoire():
	for lettre in lettres:
		lettre.aleatoire = true

var compteur = 0
var compteur_aleatoire = 0

###################
# *
###################
func _process(delta):
	compteur += delta
	compteur_aleatoire += delta
	
	if doit_ecrire and compteur_aleatoire > 1:
		# affichage "triangulaire"
		if compteur >0.02:
			if index_courant< TAILLE_H:
				# on affiche une lettre de la première ligne
				if index_courant < phrase.length():
					lettres[index_courant].lettre(phrase[index_courant])
				else:
					lettres[index_courant].lettre("")
				
				# puis on calcule les emplacements à distance constante de cette lettre pour chaque ligne
				var suivant = index_courant + (TAILLE_H-1)
				while suivant < TAILLE_H * TAILLE_V :#and suivant%(TAILLE_H) <= index_courant:
					# deuxième condition : pour éviter de dépasser le triangle
					if suivant < phrase.length():
						lettres[suivant].lettre(phrase[suivant])
					else:
						lettres[suivant].lettre("")
					suivant = suivant + (TAILLE_H-1)
				
				index_courant+=1
			else:
				$Ecriture.playing=false
				doit_ecrire = false
	
			compteur = 0
		
	if !doit_ecrire and Input.is_action_just_pressed("espace"):
		if message_courant < messages.size():
			ecrire(messages[message_courant])
			message_courant += 1 
		else:
			emit_signal("fin")

###################
# *
###################		
func casse():
	$Node2D/Reflets.frame = 2
