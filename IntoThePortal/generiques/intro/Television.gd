extends Node2D

var informations_continues = ["Bonjour","Mais qu'est-ce que qui se passe ?","Sur mon fier destroyer."]
var index_informations_continues

func _ready():
	index_informations_continues = 0
	$EnContinu/Label/Tween.connect("tween_completed",self,"enContinu")
	enContinu()

func enContinu(truc1=null,truc2=null):

	
	$EnContinu/Label.text = informations_continues[index_informations_continues]
	var taille_texte = $EnContinu/Label.text.length() * 20
	$EnContinu/Label/Tween.interpolate_property($EnContinu/Label,"margin_left",1000,-taille_texte,$EnContinu/Label.text.length(),Tween.TRANS_LINEAR,Tween.EASE_OUT)
	$EnContinu/Label.margin_left = -taille_texte
	$EnContinu/Label/Tween.start()
	index_informations_continues = (index_informations_continues+1)%informations_continues.size()

	$EnContinu.show()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
