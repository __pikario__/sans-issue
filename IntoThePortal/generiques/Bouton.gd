extends Node2D

signal active

func _on_Area2D_body_entered(body):
	if body.has_method("move_and_collide"):
		emit_signal("active")
		$Sprite.frame=1
		$Clic.play()
	if body.is_in_group("boites") and body.viens_de_sauter:
		body.sauter(self.global_position, self.global_position + body.direction_saut * 128)
	
func _on_Area2D_body_exited(body):
	if body.has_method("move_and_collide"):
		$Sprite.frame=0
		$Declic.play()