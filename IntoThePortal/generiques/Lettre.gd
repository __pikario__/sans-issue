extends Node2D

var aleatoire = false

func lettre(lettre):
	aleatoire = false
	$Label.text = lettre
	
func animation(booleen):
	if booleen:
		$AnimationPlayer.play("affichage")
	else:
		$AnimationPlayer.stop()
		
var compteur = 0		
func _process(delta):
	if aleatoire and compteur>0.05:
		$Label.text = char(randi()%127)
		compteur = 0
	compteur += delta