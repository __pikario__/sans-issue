extends Area2D

signal selection
signal posee
signal enlevee

signal disparition
signal entree
signal sortie

# si la tuile est du vide ou non
var existe = true
# s'il resta au joueur des trampolines à placer
var trampoline_possible = true
# si le mode construction est activé
var mode_construction = false
# si la tuile peut être modifiée dans le mode construction
var fixe = false

var limite_depassee = false

#########
# *
#########
func _ready():
	$Miniature.scale = Vector2(0.7,0.7)

#########
# *
#########
func taille():
	return $Sprite.texture.get_size() / Vector2($Sprite.hframes,$Sprite.vframes)

#########
# clic de la souris à l'intérieur
#########
func _on_Tuile_input_event(_viewport, event, _shape_idx):
	# s'il s'agit d'un événement lié à la souris
	if event is InputEventMouseButton:
		# s'il sagit d'un bouton pressé (et non relaché)
		# s'il s'agit du bouton gauche
		if event.pressed and event.button_index == BUTTON_LEFT:
			if trampoline_possible and self.existe: 
				emit_signal("selection",self,event.position)
			if mode_construction and not self.fixe:
				if !self.existe:
					if not self.limite_depassee:
						self.existe = true
						$AnimationPlayer.play("posage")
						$Posage.play()
						emit_signal('posee')
				else:
					self.existe = false
					$AnimationPlayer.play("enlevage")
					emit_signal('enlevee')
					$Enlevage.play()
					# si un objet était dessus il tombe
					for o in self.get_overlapping_bodies():
						if o.has_method("tomber"):
							o.tomber()
					
#########
# survol de la souris entrée
#########
func _on_Tuile_mouse_entered():
	if not self.mode_construction and self.trampoline_possible and self.existe:
		$Indicateur.show()
	if self.mode_construction and not self.limite_depassee and not self.existe:
		$Miniature.show()

#########
# survol de la souris sortie
#########	
func _on_Tuile_mouse_exited():
	$Indicateur.hide()
	if $AnimationPlayer.current_animation != "posage": 
		$Miniature.hide()

#########
# *
#########
func indiquer():
	var direction_clic = get_viewport().get_mouse_position()-self.get_global_transform_with_canvas().origin
	var angle = direction_clic.normalized().round()
	if angle.y != 0 && angle.x != 0:
		angle.x = 0
	$Indicateur.rotation = angle.rotated(PI/2).angle()

#########
# *
#########
func apparition(booleen):
	# apparition
	if booleen:
		self.existence(null,true)
		$AnimationPlayer.play("apparition")
		if($AnimationPlayer.is_connected("animation_finished",self,"existence")):
			$AnimationPlayer.disconnect("animation_finished",self,"existence")
	# disparition
	else:
		$AnimationPlayer.play_backwards("apparition")
		$AnimationPlayer.connect("animation_finished",self,"existence",[false],CONNECT_ONESHOT)
		emit_signal("disparition")
		# tout ce qui était dessus doit tomber
		for body in get_overlapping_bodies():
			if body.has_method("tomber"):
				body.tomber()
				
	self.existe = booleen
	
#########
# *
#########
func existence(_truc,booleen):
	$Sprite.visible = booleen
	self.existe = booleen

#########
# *
#########
func _on_Tuile_body_entered(body):
	if !self.existe and body.has_method("tomber"):
		body.tomber()
	emit_signal('entree',body)

#########
# *
#########
func _on_Tuile_body_exited(body):
	emit_signal('sortie',body)

#########
# *
#########
func _process(_delta):
	var estOccupe = false
	for enfant in get_children():
		if enfant.is_in_group("occupant"):
			estOccupe = true
		
	if estOccupe:
		if self.is_connected("mouse_entered",self,"_on_Tuile_mouse_entered"):
			self.disconnect("mouse_entered",self,"_on_Tuile_mouse_entered")
		if self.is_connected("mouse_exited",self,"_on_Tuile_mouse_entered"):
			self.disconnect("mouse_exited",self,"_on_Tuile_mouse_exited")
		if self.is_connected("input_event",self,"_on_Tuile_input_event"):
			self.disconnect("input_event",self,"_on_Tuile_input_event")
		
	if $Indicateur.visible:
		indiquer()

#########
# *
#########
func trampoline(booleen):
	trampoline_possible = booleen

#########
# *
#########
func construction(booleen):
	# désactivation des trampolines en mode construction
	self.trampoline_possible = !booleen
	
	# affichage du selecteur en mode construction pour les cases vides
	if !self.fixe:
		self.mode_construction = booleen
		$Selecteur.visible = booleen
	else:
		$Cadena.visible = booleen
		
