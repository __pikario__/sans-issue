extends "res://generiques/Acte1.gd"

signal fin
signal tomber
signal acte2
signal couper_musique

var VIDE = load("res://generiques/Vide.tscn")
var JOUEUR  = load("res://generiques/Personnage.tscn")

const TAILLE_TUILE = 64
const OFFSET = 32

var joueur
var trangression = false

#########
# *
#########
func _ready():
	# On ne peut pas poser de trampoline dans ce couloir
	get_tree().call_group("tuiles","trampoline",false)
	souris.pas_de_curseur()

	if !trangression:
		# Placement de tuiles de vide
		for x in range(-8,9):
			for y in range(-4,5):
				if abs(x) > 1:
					var vide = VIDE.instance()
					vide.position = Vector2(x,y) * TAILLE_TUILE - sign(x) * Vector2(OFFSET,0)
					$Vides.add_child(vide)

	# Ajout du joueur au bon endroit
	joueur = JOUEUR.instance()
	joueur.position = $Debut.position
	joueur.connect("tomber",self,"emit_signal",['tomber'])
	self.add_child(joueur)
	
	# pas de mode construction pour l'intermède
	get_tree().call_group("vides","actif",false)
	
	##### partie transgression		
	for enfant in $Transgression/Plateformes.get_children():
		if enfant.has_method("existence"):
			enfant.existence(null,false)
			enfant.connect("selection",self,"selection")

#########
# *
#########
func _process(_delta):
	# ramasse-miettes dans le cas où un trampoline du damier précédent
	#	aurait changé la souris en disparaissant après le chargement
	# 	de l'intermède
	if max_trampolines == 0 and $Tween.is_active() == false:
		get_tree().call_group("tuiles","trampoline",false)
		souris.pas_de_curseur()
		minuteur = 0

#########
# *
#########
func messages_a_afficher(messages):
	$Tuile_Afficheur.messages = messages

#########
# *
#########
#one shot
func _on_Ouverture_body_entered(body):
	if body == joueur:
		$Tuile_Afficheur.ouverture()

#########
# *
#########
func _on_Fin_body_entered(body):
	if body == joueur:
		$Tween.connect("tween_all_completed",self,"emit_signal",["fin"],CONNECT_ONESHOT)
		$Tween.interpolate_property(self, "modulate:a",
			1.0, 0.0, 0.7,
			Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()

#########
# *
#########
func _on_Tuile_Afficheur_debut_affichage():
	self.joueur.desactiver(true)

#########
# *
#########
func _on_Tuile_Afficheur_fin_affichage():
	self.joueur.desactiver(false)

#########
# fonction appelée après le dernier niveau
# permet de mener le joueur à l'acte 2
#########
func transgression():
	get_tree().call_group("tuiles","trampoline",true)

	#$Ouverture/CollisionShape2D.disabled = true
	$Transgression/Plateformes/Tr1.apparition(true)
	$Transgression/Plateformes/Tr1.connect("entree",self,"tr_entree",["Tr2"])
	$Transgression/Plateformes/Tr1.connect("sortie",self,"tr_sortie")
	
	get_tree().call_group("tuiles","connect","selection",self,"selection")
	souris.trampoline_dispo()
	max_trampolines = 1
	
	emit_signal("couper_musique")
	$Transgression/Plateformes/Tr1/Clic.play()

#########
# lorsque le joueur entre dans une tuile liée à la transgression
# fait apparaitre la tuile suivante
# 	et la connecte pour l'apparition de celle d'après
#########
func tr_entree(corps,tr):
	if corps == joueur:
		var noeud = get_node("Transgression/Plateformes/"+tr)
		if not noeud.existe:
			noeud.apparition(true)
			
		var id = int(tr[-1])
		if id < $Transgression/Plateformes.get_child_count():
			noeud.connect("entree",self,"tr_entree",["Tr"+str(id+1)],CONNECT_ONESHOT)
			noeud.connect("sortie",self,"tr_sortie")

#########
# *
#########
func tr_sortie(corps):
	if self.trangression and corps.has_method('tomber'):
		corps.tomber()

#########
# *
#########
func _on_Tr5_body_exited(body):
	if body == joueur:
		if joueur.direction_saut == Vector2(-1, 0):
			$Tween.interpolate_property(self, "modulate:a",
				1.0, 0.0, 2.0,
				Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
			$Tween.start()
		
			$FinTrangression.connect("finished",self,"emit_signal",["acte2"],CONNECT_ONESHOT)
			$FinTrangression.play()

#########
# one shot
#########
func _on_Dclencheur_body_entered(body):
	if body == joueur and trangression:
		self.transgression()
		
#########
# *
#########
func afficheur_casse():
	$Tuile_Afficheur.casse()
