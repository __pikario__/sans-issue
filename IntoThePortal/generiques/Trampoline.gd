extends StaticBody2D

signal saut
signal supprime

var direction = Vector2(0,-1)

var fixe = false

#
func _ready():
	$Poser.play()

#
func _on_Area2D_body_entered(body):
	if body.has_method("move_and_collide"):
		emit_signal("saut",self,body,direction)
		
# au survol		
func _on_Trampoline_mouse_entered():
	if not fixe and $NonSupprimable.is_stopped():
		self.modulate = Color(1,0,0,1)
	
# au survol
func _on_Trampoline_mouse_exited():
	if not fixe:
		self.modulate = Color(1,1,1,1)

# appui sur un trampoline
func _on_Trampoline_input_event(_viewport, event, _shape_idx):
	if not fixe:
		if event is InputEventMouseButton and event.pressed:
			if event.button_index == BUTTON_LEFT:
				emit_signal("supprime",self)

func sens(clic):
	var direction_clic = clic-self.get_global_transform_with_canvas().origin
	var angle = direction_clic.normalized().round()
	if angle.y != 0 && angle.x != 0:
		angle.x = 0
	$Sprite.rotation = angle.rotated(PI/2).angle()
	self.direction = angle
