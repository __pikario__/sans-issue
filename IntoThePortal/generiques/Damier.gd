extends "res://generiques/Acte1.gd"

signal suivant
signal echec

signal compteur_construction

var JOUEUR = load("res://generiques/Personnage.tscn")

var BOUTON = load("res://generiques/Bouton.tscn")
var RECEPTION = load("res://generiques/Reception.tscn")
var INTERRUPTEUR = load("res://generiques/Interrupteur.tscn")
var SONNETTE = load("res://generiques/Sonnette.tscn")

var BOITE = load("res://generiques/Boite.tscn")

var taille_grille
var decalage = Vector2(0,1)
var marge_vide = 4
var camera_active = true
var joueur_present = true
var entree_sortie = true

var intitule = "En toute chose les débuts sont modestes"

var boites = {}
var interrupteurs = []
var tuiles_sortie = []
var tuiles_entree = []

var v_boutons = [Vector2(0,1),Vector2(4,5)]
var v_receptions = [Vector2(2,1),Vector2(4,4)]
var v_interrupteurs = [Vector2(5,5)]
var v_sonnettes = [Vector2(5,5)]
var v_sonnables = []
var v_vides = {3:[0,1,2,3,4,5]}
var v_trampolines = []

var niveau = {}

var construites = 0

#########
# *
#########
func _ready():
	set_process(false)
	
	# on met l'intitule au niveau
	$CanvasLayer/Intitule.text = tr(self.intitule)

	# activation ou non de la caméra
	$Camera2D.current = self.camera_active
	
	## initialisation du tableau du niveau
	for x in range(-marge_vide,taille_grille+marge_vide):
		niveau[float(x)]={}
		for y in range(-marge_vide,taille_grille+marge_vide):
			niveau[float(x)][float(y)] = null
			
	for x in range(-marge_vide,taille_grille+marge_vide):
		for y in range(-marge_vide,taille_grille+marge_vide):
			# les vides ont été remplacé par des tuiles inexistantes
			#var vide = VIDE.instance()
			var vide = TUILE.instance()
			vide.existence(null,false)
			self.placer(vide,Vector2(x,y))
	
	if entree_sortie:
		## entrée
		for x in range(taille_grille/2-1,taille_grille/2+1):
			for y in range(taille_grille,taille_grille+1):
				var tuile = TUILE.instance()
				self.placer(tuile,Vector2(x,y))
				tuiles_entree.append(tuile)
		var depart = Area2D.new()
		var collision_depart = CollisionShape2D.new()
		collision_depart.shape = RectangleShape2D.new()
		collision_depart.shape.extents = Vector2(2,1) * taille_tuile/2
		depart.add_child(collision_depart)
		self.placer(depart,Vector2(taille_grille/2-0.5,taille_grille))
		depart.connect("body_exited",self,"disparition_entree",[],CONNECT_ONESHOT)
		
		## sortie
		for x in range(taille_grille/2-1,taille_grille/2+1):
			for y in range(-6,0):
				var tuile = TUILE.instance()
				self.placer(tuile,Vector2(x,y))
				tuile.existence(null,false)
				tuiles_sortie.append(tuile)
		var arrivee = Area2D.new()
		var collision_arrivee = CollisionShape2D.new()
		collision_arrivee.shape = RectangleShape2D.new()
		collision_arrivee.shape.extents = Vector2(2,1) * taille_tuile/2
		arrivee.add_child(collision_arrivee)
		self.placer(arrivee,Vector2(taille_grille/2-0.5,-3))
		arrivee.connect("body_exited",self,"suivant")
		
		# placement du joueur
		if joueur_present:
			var joueur = JOUEUR.instance()
			joueur.position = depart.position
			joueur.connect("tomber",self,"echec")
			self.call_deferred("add_child",joueur)
	
	# grille principale
	for x in range(taille_grille):
		for y in range(taille_grille):
			if !v_vides.has(y) or !v_vides[y].has(x):
				var tuile = TUILE.instance()
				tuile.fixe = true
				self.placer(tuile,Vector2(x,y))
				
	for cellule in v_interrupteurs:
		var interrupteur = INTERRUPTEUR.instance()
		self.placer(interrupteur,cellule)		
		interrupteur.connect("active",self,"verification_interrupteurs")
		interrupteur.connect("desactive",self,"verification_interrupteurs")
		interrupteurs.append(interrupteur)
					
	for i in range(v_receptions.size()):
		var reception = RECEPTION.instance()
		self.placer(reception,v_receptions[i])
		reception.connect("active",self,"apparition_boite",[reception])
		
		if i < v_boutons.size():
			reception.modulate.b -= i*0.5
			
			var bouton = BOUTON.instance()
			self.placer(bouton,v_boutons[i])
			bouton.modulate = reception.modulate
			bouton.connect("active",reception,"active")
		else:
			reception.modulate.b = 1
			reception.modulate.r = 1
			
			self.apparition_boite(reception,true)
		

	for i in range(v_sonnettes.size()):
		var sonnette = SONNETTE.instance()
		self.placer(sonnette,v_sonnettes[i])
		sonnette.modulate.g -= i*0.2
		
		var tuiles = []
		for cellule in v_sonnables[i]:
			var tuile = TUILE.instance()
			tuile.modulate = sonnette.modulate
			tuile.existence(null,false)
			tuile.fixe=true
			self.placer(tuile,cellule)
			tuile.modulate = sonnette.modulate
			tuiles.append(tuile)
		sonnette.connect("active",self,"sonner",[true,tuiles])
		sonnette.connect("desactive",self,"sonner",[false,tuiles])
	
	# désactivation du placement de trampolines pour les tuiles
	if	trampolines.size() >= max_trampolines:
		get_tree().call_group("tuiles","trampoline",false)
		
	# trampolines déjà présents
	# cas de la salle de construction 1
	for v in self.v_trampolines:
		var tuile = niveau[v[0].x][v[0].y]
		var clic = v[1]
		selection(tuile,clic)

#########
# fonction générique qui place un élément sur la grille du niveau
#########
func placer(element,cellule):
	# d'abord on s'occupe du placement du sol 
	
	# une tuile blanche
	if(element.get_filename() == TUILE.get_path()):
		if niveau.has(cellule.x) and niveau[cellule.x].has(cellule.y) and niveau[cellule.x][cellule.y] != null:
			self.remove_child(niveau[cellule.x][cellule.y])
		niveau[cellule.x][cellule.y] = element

		element.connect("selection",self,"selection")
		# en mode contruction, tient un compte des tuiles construites
		element.connect("posee",self,"maj_construction",[+1])
		element.connect("enlevee",self,"maj_construction",[-1])
		
	# puis du placement des éléments
	if(element.is_in_group("occupant")):
		niveau[cellule.x][cellule.y].add_child(element)
		#niveau[cellule.x][cellule.y].call_deferred("add_child",element)
	else:
		#self.call_deferred("add_child", element)
		self.add_child(element)
		
	# on positionne l'élément
	element.global_position = (cellule - Vector2(taille_grille/2,taille_grille/2) + decalage)*taille_tuile

#########
# pour tenir à jour le compteur des tuiles construites en mode construction
#########
func maj_construction(positif_negatif):
	self.construites += positif_negatif
	emit_signal('compteur_construction',self.construites)

#########
# renvoie la position de la tuile au centre de la grille
#########
func centre():
	var centre = Vector2(taille_grille/2,taille_grille/2)
	return niveau[centre.x][centre.y].global_position

#########
# fait apparaître une boîte à une réception voulue
#########
func apparition_boite(source,auto=false):
	# si une boite est déjà associée à cette réception
	if boites.has(source):
		# on la libère
		boites[source].queue_free()
		
	# crétion d'une nouvelle boite
	var boite = BOITE.instance()
	# ajout à la réception
	boite.position = source.position
	self.call_deferred("add_child",boite)
	boites[source]=boite
	
	# animation
	boite.call_deferred("apparition")
	
	# si pas de bouton associé, la boîte réapparait dès qu'elle tombe
	if auto:
		boite.connect("tomber",self,"apparition_boite",[source,true])

#########
# faire tomber les tuiles du bas de l'écran
#########
func disparition_entree(_truc):
	for tuile in tuiles_entree:
		tuile.apparition(false)
	$CanvasLayer/AnimationPlayer.play_backwards("intro")
	
	# si on peut poser un trampoline affichage du tuto
	if max_trampolines > 0:
		tuto.afficher_poser_trampoline()
	
#########
# *
#########		
# active des tuiles temporaires
func sonner(booleen,tuiles):
	for tuile in tuiles:
		tuile.apparition(booleen)

#########
# vérifier si tous les interrupteurs nécéssaires à l'ouverture de la porte sont activés
#########
func verification_interrupteurs():
	for interrupteur in interrupteurs:
		if !interrupteur.est_active():
			self.resolu(false)
			return
	self.resolu(true) 
	
#########
# fait apparaître la sortie lorsque la salle est résolue
#########
func resolu(booleen):
	for tuile in tuiles_sortie:
		tuile.apparition(booleen)

#########
# fin de la salle
#########
func suivant(_truc):
	$Tween.connect("tween_all_completed",self,"emit_signal",["suivant"],CONNECT_ONESHOT)
	$Tween.interpolate_property(self, "modulate:a",
		1.0, 0.0, 0.7,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()
func echec():
	emit_signal("echec")

#########
# *
#########
func activer_mode_construction(booleen):
	if booleen:
		souris.normal()
	else:
		if trampolines.size() > max_trampolines-1:
			souris.trampoline_epuise()
		else:
			souris.trampoline_dispo()
	self.mode_construction = booleen
	if get_tree() != null:
		get_tree().call_group("tuiles","construction",booleen)
	for trampoline in self.trampolines:
		trampoline.fixe = booleen
