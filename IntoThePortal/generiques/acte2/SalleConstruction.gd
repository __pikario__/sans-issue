extends Node2D

signal echec
signal salle_suivante

var DAMIER = load("res://generiques/Damier.tscn")

var damier

var max_construction = 2

#########
# *
#########
func _ready():
	$SurinterfaceConstruction/TuilesRestantes.text = 'tuiles restantes : '+str(self.max_construction) 
	
#########
# *
#########
func ajout_damier(donnees_damier):
	self.damier = DAMIER.instance()
	self.damier.taille_grille = 6
	self.damier.marge_vide = 0
	self.damier.camera_active = false
	self.damier.joueur_present = false
	self.damier.entree_sortie = false
	
	self.damier.intitule = donnees_damier['intitule']
	self.damier.v_boutons = donnees_damier['boutons']
	self.damier.v_receptions = donnees_damier['receptions']
	self.damier.v_interrupteurs = donnees_damier['interrupteurs']
	self.damier.v_vides = donnees_damier['vides']
	self.damier.v_sonnettes = donnees_damier['sonnettes']
	self.damier.v_sonnables = donnees_damier['sonnables']
	self.damier.max_trampolines = donnees_damier['max_trampolines']
	if donnees_damier.has("trampolines"):
		self.damier.v_trampolines = donnees_damier['trampolines']
	
	self.add_child(self.damier)
	self.damier.position = $Emplacement_Damier.position
	self.damier.connect('compteur_construction',self,'compteur_construction')
	
	self.damier.get_node("Neant").hide()
	
#########
# *
#########
func compteur_construction(nombre):
	$SurinterfaceConstruction/TuilesRestantes.text = 'tuiles restantes : '+str(self.max_construction - nombre) 
	get_tree().set_group('tuiles','limite_depassee',nombre >= self.max_construction)

#########
# *
#########
func vue_d_ensemble(active):
	# modification l'animation de caméra en fonction de l'emplacement du damier
	var anim = $Personnage/Camera2D/AnimationPlayer.get_animation("dezoom")
	var centre = $Personnage.transform.xform_inv(self.damier.centre())
	anim.track_set_key_value(1,1,centre)
	anim.track_set_key_value(0,1,Vector2(1.1,1.1))
	
	if active:
		$Personnage/Camera2D/AnimationPlayer.play("dezoom")
		$SurinterfaceConstruction/ModeAccompagne.show()
		$SurinterfaceConstruction/TuilesRestantes.show()
	else:
		$Personnage/Camera2D/AnimationPlayer.play_backwards("dezoom")
		$SurinterfaceConstruction/ModeAccompagne.hide()
		$SurinterfaceConstruction/TuilesRestantes.hide()

#########
# *
#########
func _on_PanneauControle_actif(booleen):
	self.vue_d_ensemble(booleen)
	self.damier.activer_mode_construction(booleen)
	if booleen:
		$SurinterfaceConstruction/RapportStage.debut()

#########
# *
#########
func _on_Sortie_body_entered(body):
	if body == $Personnage:
		$Tween.connect("tween_all_completed",self,"emit_signal",["salle_suivante"],CONNECT_ONESHOT)
		$Tween.interpolate_property(self, "modulate:a",
			1.0, 0.0, 2.0,
			Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()

#########
# *
#########
func _on_Personnage_tomber():
	emit_signal("echec")
