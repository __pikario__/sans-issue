extends Control

var index = 0
var morceaux = []
var id = -1

onready var tuto = get_node("/root/Tuto")

###################
# *
###################
func debut():
	# rétablissement de la sauvegarde entre les gameover
	if tuto.deja_mail[self.id]:
		index = morceaux.size()
		
	if not morceaux.empty() and index < morceaux.size():
		self.show()
		get_tree().call_group("personnage","desactiver",true)
		$Tween.interpolate_property(self,'modulate',Color(1,1,1,0),Color(1,1,1,1),2,Tween.TRANS_LINEAR,Tween.EASE_IN)
		deroule()

###################
# affichage du morceau suivant
###################
func deroule():
	$Label.text = tr(morceaux[index])
	$Tween.interpolate_property($Label,'modulate',Color(1,1,1,0),Color(1,1,1,1),3,Tween.TRANS_LINEAR,Tween.EASE_IN)
	$Tween.start()
	index += 1

###################
# *
###################
func _process(_delta):
	if Input.is_action_just_pressed("espace") and (not $Tween.is_active() or $Tween.tell() > 0.2):
		
		var temps = 1
		if $Tween.is_active():
			$Tween.remove($Label,"modulate")
			temps = 0.1
			
		$Tween.interpolate_property($Label,'modulate',$Label.modulate,Color(1,1,1,0),temps,Tween.TRANS_LINEAR,Tween.EASE_IN)
		$Tween.start()
		
		# quand le fondu est fini
		if index < morceaux.size():
			if not $Tween.is_connected('tween_all_completed',self,'deroule'):
				$Tween.connect('tween_all_completed',self,'deroule',[],CONNECT_ONESHOT)
		else:
			if not $Tween.is_connected('tween_all_completed',self,'hide'):
				$Tween.connect('tween_all_completed',self,'fin',[],CONNECT_ONESHOT)

###################
# *
###################	
func fin():
	self.hide()
	get_tree().call_group("personnage","desactiver",false)
	tuto.deja_mail[self.id] = true
