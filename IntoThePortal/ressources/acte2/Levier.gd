extends Node2D

signal active

func _on_Area2D_body_entered(_body):
	$AudioStreamPlayer2D.play()
	$AnimationPlayer.play("activer")
	emit_signal("active")
