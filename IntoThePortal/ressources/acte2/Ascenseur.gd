extends Node2D

const VOLUME_MIN = -60
var volune_initial
var fondue_faite = false

var decalage = Vector2(-28,-28)

var casse = false
var essais_casse = 0

export var acces_courant = 0

var passager
var position_passager_locale_ascenceur

func _ready():
	volune_initial = $Deplacement.volume_db

func trajet():
	
	# son
#	$Tween.interpolate_property($Deplacement, "volume_db", VOLUME_MIN, volune_initial, 0.01, 1, Tween.EASE_IN, 0)
#	$Tween.start()
	$Deplacement.volume_db = volune_initial
	$Deplacement.play()
	
	# on ferme les barrières
	$Acces0/StaticBody2D/CollisionShape2D.disabled = false
	$Acces1/StaticBody2D/CollisionShape2D.disabled = false
	
	# on désactive la collision du centre
	#$Corps/Centre/Area2D/CollisionShape2D.disabled = true
	
	var anim = $AnimationPlayer.get_animation("trajet")
	var depart = get_node("Acces"+str(acces_courant))
	
	# alternance de l'accès où se trouve l'ascenseur
	acces_courant = (acces_courant +1) %2 
	
	var arrivee = get_node("Acces"+str(acces_courant))

	anim.track_set_key_value(0,0,depart.position)
	anim.track_set_key_value(0,1,arrivee.position)
	var direction = Vector2(cos(arrivee.rotation),sin(arrivee.rotation)).ceil()

	ferme()

	$AnimationPlayer.play("trajet")
	
	var arg = ""
	if(direction == Vector2(0,1)):
		arg="haut"
	elif(direction == Vector2(1,0)):
		arg="gauche"
	elif(direction == Vector2(1,-1)):
		arg="bas"
	elif(direction == Vector2(-1,1)):
		arg="droite"	
		
	$AnimationPlayer.connect("animation_finished",self,"trajet_fini",[arg],CONNECT_ONESHOT)
	
	fondue_faite = false
	set_process(true)
	
func trajet_fini(_truc,arg):
	ouvert(arg)
	# on désactive la barrière liée à l'arrivée et on réactive celle liée au départ
	$Acces0/StaticBody2D/CollisionShape2D.disabled = acces_courant==0
	$Acces1/StaticBody2D/CollisionShape2D.disabled = acces_courant==1
	
	# on réactive la collision du centre
	#$Corps/Centre/Area2D/CollisionShape2D.disabled = false
	
	if self.passager and self.passager.has_method("desactiver"):
		self.passager.desactiver(false)
	set_process(false)
	
func _process(_delta):
	if self.passager:
		self.passager.global_position = $Corps/Centre.global_transform.xform(self.position_passager_locale_ascenceur)
		
	if $AnimationPlayer.is_playing() and not fondue_faite and $AnimationPlayer.current_animation_length - $AnimationPlayer.current_animation_position < 0.3:
		$Tween.interpolate_property($Deplacement, "volume_db", $Deplacement.volume_db, VOLUME_MIN, 1, 1, Tween.EASE_IN, 0)
		$Tween.start()
		$Tween.connect("tween_completed",$Deplacement,"stop",[],CONNECT_ONESHOT)
		fondue_faite = true

func ferme():
	$Corps/Haut.visible = true
	$Corps/Haut/StaticBody2D/CollisionShape2D.disabled = false
	$Corps/Bas.visible = true
	$Corps/Bas/StaticBody2D/CollisionShape2D.disabled = false
	$Corps/Gauche.visible = true
	$Corps/Gauche/StaticBody2D/CollisionShape2D.disabled = false
	$Corps/Droit.visible = true
	$Corps/Droit/StaticBody2D/CollisionShape2D.disabled = false

func ouvert(acces):
	ferme()
	if acces == "haut":
		$Corps/Haut.visible = false
		$Corps/Haut/StaticBody2D/CollisionShape2D.disabled = true
		$Corps/Centre.frame = 0
	elif acces == "bas":
		$Corps/Bas.visible = false
		$Corps/Bas/StaticBody2D/CollisionShape2D.disabled = true
		$Corps/Centre.frame = 2
	elif acces == "gauche":
		$Corps/Gauche.visible = false
		$Corps/Gauche/StaticBody2D/CollisionShape2D.disabled = true
		$Corps/Centre.frame = 3
	elif acces == "droite":
		$Corps/Droit.visible = false
		$Corps/Droit/StaticBody2D/CollisionShape2D.disabled = true
		$Corps/Centre.frame = 1
	
func _on_Levier_active():
	if not casse:
		trajet()
	else:
		# finalement on le casse tout de suite 
		# 	-> retour de Thomas qui cherchait à le réactiver
		if essais_casse == 0:
			$AnimationPlayer.play("detruit")
			$Casse.play()
		essais_casse += 1
			

func _on_Area2D_body_entered(body):
	if self.passager == null and body.is_in_group("personnage"):
		trajet()
		self.passager=body
		self.position_passager_locale_ascenceur = $Corps/Centre.global_transform.xform_inv(self.passager.global_position)
		if self.passager.has_method("desactiver"):
			self.passager.desactiver(true)
		set_process(true)

func _on_Perimetre_body_exited(body):
	if body.is_in_group("personnage"):
		self.passager = null
