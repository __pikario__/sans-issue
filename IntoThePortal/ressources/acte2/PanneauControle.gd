extends Node2D

signal actif

func _on_Area2D_body_entered(body):
	if body.is_in_group("personnage"):
		$AnimationPlayer.play("ouverture")
		$AnimationPlayer.connect("animation_finished",$AnimationPlayer,"play",["fonctionnement"],CONNECT_ONESHOT)
		emit_signal("actif",true)

func _on_Area2D_body_exited(body):
	if body.is_in_group("personnage"):
		$AnimationPlayer.play_backwards("ouverture")
		emit_signal("actif",false)
