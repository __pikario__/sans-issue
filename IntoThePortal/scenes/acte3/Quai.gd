extends Node2D

var cestparti = false
var index_path = 0

var courbes = []
var transformations = []

#########
# *
#########	
func _ready():
	if get_parent() == get_tree().root:
		$Camera2D.current = true
		
	for chemin in $Chemins.get_children():
		courbes.append(chemin.curve)
		
	for i in $Passants.get_child_count():
		if i < $Chemins.get_child_count():
			var enfant = $Chemins.get_child(i)
			$Passants.get_child(i).definir_courbe_et_transform(enfant.curve,enfant.global_transform)
			
	for chemin in $Chemins.get_children():
		$Chemins.remove_child(chemin)

#########
# *
#########
func entree_passants():
	cestparti = true
	for i in $Passants.get_child_count():
		$Passants.get_child(i).marcher = true
	
