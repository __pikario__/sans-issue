extends Node2D

signal sansissue

###################
# *
###################
func _ready():
	$CanvasModulate.color = Color.black
	$CanvasModulate.show()
	$Couloirs.position=Vector2(0,0)
	$Quai.position = Vector2(-2000,0)

###################
# *
###################
func _on_RameMetro_depart():
	$Decor.play("depart")
	$RameMetro.connect("arrivee",self,"arrivee_en_gare",[],CONNECT_ONESHOT)
	$Personnage/Light2D.hide()

###################
# *
###################
func arrivee_en_gare():
	$Decor.play("arrivee_en_gare")
	$Decor.connect("animation_finished",self,"ouverture_porte", [], CONNECT_ONESHOT)

###################
# *
###################
func ouverture_porte(_truc):
	$Quai.entree_passants()
	$RebourArret.start()

###################
# *
###################
func _on_RebourArret_timeout():
	$RameMetro.depart_fin()
	
	for passant in $Quai/Passants.get_children():
		var gpos = passant.global_position
		passant.set_physics_process(false)
		$Quai/Passants.remove_child(passant)
		$RameMetro.add_child(passant)
		passant.global_position = gpos
		passant.desactiver(true)
		
	$Decor.play("depart_en_gare")
	$RebourFin.start()	

###################
# *
###################
func _on_RebourFin_timeout():
	$RameMetro/Animation_Klaxon.play("klaxon")
	$RameMetro/Klaxon.play()
	$RebourGenerique.start()

###################
# *
###################
func _on_RebourGenerique_timeout():
	emit_signal("sansissue")
