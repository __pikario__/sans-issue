extends Control

signal ecran_titre

var index = -1
const CARTONS = [
	"GENERIQUE-TITRE",
	"GENERIQUE-PIKARIO",
	"GENERIQUE-MUSIQUES1",
	"GENERIQUE-MUSIQUES2",
	"GENERIQUE-MUSIQUES3",
	"GENERIQUE-MUSIQUE-ENIGME",
	"GENERIQUE-MUSIQUEDEFIN",
	"GENERIQUE-EXTRAITS",
	"GENERIQUE-TRADUCTION",
	"GENERIQUE-TESTEURS",
	"GENERIQUE-LOGICIEL",
	"GENERIQUE-LICENCES",
]

onready var souris = get_node("/root/Souris")

###################
# *
###################
func _ready():
#	TranslationServer.set_locale("en")
	self.hide()
#	generique()

###################
# *
###################
func generique():
	self.show()
	
	index = -1
	carton_suivant()

###################
# *
###################
func carton_suivant():
	index+=1
	if index < CARTONS.size():
		$Timer.start()
		$Texte.text = tr(CARTONS[index])
	else:
		$Texte.text = ""
		souris.normal()
		$Button.show()

###################
# *
###################
func _on_Timer_timeout():
	carton_suivant()

###################
# *
###################
func _on_Button_pressed():
	emit_signal("ecran_titre")
