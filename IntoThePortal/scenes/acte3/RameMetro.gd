extends Node2D

signal depart
signal arrivee

# la fin
signal sansissue

var chrono = 0

const AUQUAI = -2
const DEPART = -1
const ARRET = 0
const ENMARCHE = 1

var etat_rame = DEPART

const TEMPS_TRAJET = 4

###################
# ferme les portes en utilisant la TileMap
###################
func fermer_acces():
	var id_murinvisible = $Murs.tile_set.find_tile_by_name("MurInvisible")
	$Murs.set_cell(8,5,id_murinvisible)
	$Murs.set_cell(9,5,id_murinvisible)
	$Murs.set_cell(2,5,id_murinvisible)
	$Murs.set_cell(3,5,id_murinvisible)
	$Murs.set_cell(4,5,id_murinvisible)

###################
# *
###################
func _process(delta):
	chrono+=delta
	
	match etat_rame: 
		ENMARCHE:
			# démarrage
			if chrono > 3 and $Exterieur.playback_speed < 2.6:
				$Exterieur.playback_speed += 0.2
				chrono = 0
				
			# arrêt 
			elif chrono > TEMPS_TRAJET and ENMARCHE == etat_rame:
				etat_rame = ARRET
				$Freinage.play()
		ARRET:
			if chrono > 1 and $Exterieur.playback_speed > 0:
				$Exterieur.playback_speed -= 0.2
				chrono = 0
				$EnMarche.volume_db -= 1
				
			if $Exterieur.playback_speed < 0.5:
				if not $Annonce.playing:
					$Annonce.play()
				
			if $Exterieur.playback_speed < 0.2:
				arrivee()
				$TunnelLumiere.hide()
				$TunnelLumiere/Light2D2.hide()
				$EnMarche.stop()
				$Freinage.stop()
				etat_rame = AUQUAI
			

###################
# *
###################
func _on_Declencheur_body_entered(body):
	if DEPART == etat_rame and body.is_in_group("personnage"):
		get_tree().call_group("alerte","fermeture")
		$Alertes/AlerteMetro_Bas_1.connect("depart",self,"depart",[],CONNECT_ONESHOT)
		
		fermer_acces()

###################
# *
###################	
func depart():
	etat_rame = ENMARCHE
	$Exterieur.playback_speed = 1
	$Exterieur.get_animation("enmouvement").loop = true
	$Exterieur.play("enmouvement")
	emit_signal("depart")
	$EnMarche.volume_db = 0
	$EnMarche.play()
	
###################
# *
###################
func arrivee():
	emit_signal("arrivee")
	$Exterieur.stop()
	# ouvre l'accès
	$Murs.set_cell(8,5,-1)
	$Murs.set_cell(9,5,-1)
	$Murs.set_cell(2,5,-1)
	$Murs.set_cell(3,5,-1)
	$Murs.set_cell(4,5,-1)
	$Portes.play()

###################
# *
###################
func cacher_lumieres():
	$TunnelLumiere.hide()
	$TunnelLumiere/Light2D2.hide()

# inutilisé
####################
#
####################
#func fermeture_fin():
#	get_tree().call_group("alerte","fermeture")
#	$Alertes/AlerteMetro_Bas_1.connect("depart",self,"depart_fin",[],CONNECT_ONESHOT)

###################
# spécifique à la fin du jeu
###################
func depart_fin():
	$Portes.play()
	fermer_acces()
	
	$Exterieur.playback_speed = 3
	$EnMarche.volume_db = 0
	$TunnelLumiere/Light2D2.hide()
	
	$EnMarche.play()
	$Exterieur.get_animation("enmouvement").loop = true
	$Exterieur.play("enmouvement")
	$LumiereRame.show()
