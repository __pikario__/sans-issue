extends "res://generiques/acte2/SalleConstruction.gd"

#########
# *
#########
func _ready():
	$Tween.interpolate_property(self, "modulate:a",
			0.0, 1.0, 3.0,
			Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()
	
	var donnees_damier = {'intitule':'','messages':[],'max_trampolines':2,'boutons':[],'receptions':[Vector2(4,1)],'interrupteurs':[],'sonnettes':[],'sonnables':[],'vides':{0:[0,4,5],1:[4,5],2:[1,2,3,4,5],3:[0,1,2,3,4,5],4:[0,1,2,4,5],5:[1,2]}}
	ajout_damier(donnees_damier)
		
	$Ascenseur.ouvert("haut")
	$Ascenseur/Corps/Droit.visible = false

	$SurinterfaceConstruction/RapportStage.morceaux = ['STAGIAIRE1-1','STAGIAIRE1-2','STAGIAIRE1-3','STAGIAIRE1-4']
	$SurinterfaceConstruction/RapportStage.id = 2

#########
# *
#########
func _on_Vide_body_entered(body):
	if body.is_in_group("boites"):
		body.tomber()
