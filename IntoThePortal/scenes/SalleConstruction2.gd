extends "res://generiques/acte2/SalleConstruction.gd"

#########
# *
#########
func _ready():
	var donnees_damier = {'intitule':'',
		'messages':[],
		'max_trampolines':2,
		'boutons':[],
		'receptions':[Vector2(5,1)],
		'interrupteurs':[],
		'sonnettes':[Vector2(5,5)],
		'sonnables':[[Vector2(1,5),Vector2(2,5),Vector2(1,4),Vector2(2,4)]],
		'vides':{0:[0,4,5],1:[4,5],2:[1,2,3,4,5],3:[0,1,2,3,4,5],4:[0,1,2,4,5],5:[1,2]}}
	ajout_damier(donnees_damier)
		
	$SurinterfaceConstruction/RapportStage.morceaux = ['STAGIAIRE2-1','STAGIAIRE2-2','STAGIAIRE2-3','STAGIAIRE2-4']
	$SurinterfaceConstruction/RapportStage.id = 1

#########
# *
#########
func _on_Vide_body_entered(body):
	if body.has_method('tomber'):
		body.tomber()
