extends "res://generiques/acte2/SalleConstruction.gd"

#########
# *
#########
func _ready():
	var donnees_damier = {'intitule':'',
		'messages':[],
		'max_trampolines':2,
		'boutons':[],
		'receptions':[Vector2(0,3),Vector2(5,0)],
		'interrupteurs':[],
		'sonnettes':[Vector2(1,4),Vector2(2,0)],
		'sonnables':[[Vector2(0,5)],[Vector2(5,0)]],
		'vides':{0:[0,1,4],1:[0,1,2,3,4,5],2:[0,1,2,4],3:[1,2,3,4,5],4:[0,2,3,4,5],5:[1,2,3,4,5]},
		'trampolines':[[Vector2(0,3),Vector2(139, 412)]]}
	ajout_damier(donnees_damier)

	$SurinterfaceConstruction/RapportStage.morceaux = ['COURRIEL-1','COURRIEL-2',
				'COURRIEL-3','COURRIEL-4',
				'COURRIEL-5','COURRIEL-6']
	$SurinterfaceConstruction/RapportStage.id = 0
				
	$Ascenseur.casse = true

#########
# *
#########
func _on_Area2D_body_entered(body):
	if body.has_method('sauter'):
		body.tomber_violement()

#########
# *
#########
func _on_Barriere_body_entered(body):
	if body.is_in_group('boites'):
		$Barriere/Sprite.frame = 1
		$Barriere/StaticBody2D.collision_layer = 0
		$Barriere/StaticBody2D.collision_mask = 0
		$Barriere/Cassee.play()

#########
# *
#########
func _on_Vide_body_entered(body):
	if body.has_method('tomber'):
		body.tomber()
