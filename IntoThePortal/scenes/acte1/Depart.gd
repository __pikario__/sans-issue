extends Node2D

onready var tuto = get_node("/root/Tuto")
onready var souris = get_node("/root/Souris")

signal fin

var position_initiale

#########
# *
#########
func _ready():
	
	# on ne peut pas poser de trampoline
	souris.pas_de_curseur()
	get_tree().call_group("tuiles","trampoline",false)
	
	$Personnage.connect("tomber",self,"emit_signal",["tomber"])
	position_initiale = $Personnage.global_position

#########
# *
#########
func _on_Timer_timeout():
	$AnimationPlayer.play("fermeture_porte")
	$AudioStreamPlayer2D.play()
	$TimerTuto.start()

#########
# *
#########
func _on_TimerTuto_timeout():
	tuto.afficher_deplacement()

#########
# *
#########
func _on_Fin_body_entered(body):
	if body == $Personnage:
		$Tween.connect("tween_all_completed",self,"emit_signal",["fin"],CONNECT_ONESHOT)
		$Tween.interpolate_property(self, "modulate:a",
			1.0, 0.0, 2,
			Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()

#########
# *
#########
func _on_Vide_body_entered(body):
	if body == $Personnage:
		$Personnage.tomber()

#########
# *
#########
func _on_Personnage_tomber():
	$Personnage.reinitialiser()
	$Personnage.global_position = position_initiale
