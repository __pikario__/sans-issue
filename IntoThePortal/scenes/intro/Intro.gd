extends Node2D

signal acte1

###################
# *
###################
func _ready():
	$CanvasLayer/SousTitres.text = ""

###################
# *
###################
func _on_Debut_animation_finished(anim_name):
	$Antoine.play()
	if TranslationServer.get_locale() == "en":
		$Antoine/Soustitres.play("sous-titres")
	$Tele4.show()

###################
# *
###################
func _on_Antoine_finished():
	$Tele3/ColorRect.color = Color.darkgreen
	$Edouard.play()
	if TranslationServer.get_locale() == "en":
		$Edouard/Soustitres.play("sous-titres")
	$Tele3.show()

###################
# *
###################
func _on_Edouard_finished():
	$Tele2/ColorRect.color = Color.aquamarine
	$Voisins.play()
	if TranslationServer.get_locale() == "en":
		$Voisins/Soustitres.play("sous-titres")
	$Tele2.show()
	
###################
# *
###################
func _on_Voisins_finished():
	$Tele/ColorRect.color = Color.gold
	$Macron.play()
	if TranslationServer.get_locale() == "en":
		$Macron/Soustitres.play("sous-titres")
	$Tele.show()
	$Camera2D/AnimationPlayer.playback_speed = 0.5
	
###################
# *
###################
func _on_Macron_finished():
	$Camera2D/AnimationPlayer.stop()
	$Tele/ColorRect/AnimationPlayer.stop()
	$Tele.hide()
	$CanvasLayer/SousTitres.hide()
	$CanvasLayer/Label.show()
	$Timer.start()
	$Fond.stop()
	$Fond2.stop()

###################
# *
###################
func Fond2():
	$Fond2.play()

###################
# *
###################
func _on_Timer_timeout():
	emit_signal("acte1")
	
###################
# *
###################
func _process(delta):
	var incr = $CanvasLayer/EffetTele.material.get_shader_param("scanSpeed") + delta/100;
	$CanvasLayer/EffetTele.material.set_shader_param("scanSpeed",incr)

###################
# *
###################
func soustitres(text_id = ""):
	$CanvasLayer/SousTitres.text = tr(text_id)
