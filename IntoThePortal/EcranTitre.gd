extends Control

signal nouvellepartie
signal continuer

var fichier_sauvegarde
var temps_de_jeu_humain

var langue = "fr"

###################
# *
###################
func _ready():
	var fichier = File.new()
	if not fichier.file_exists(self.fichier_sauvegarde):
		$Choix/Continuer.hide()
	$Choix/Continuer.text = tr("ECRANTITRE_CONTINUER") + " - " + str(temps_de_jeu_humain)
	
	$Choix/NouvellePartie.grab_focus()
	$PleinEcran.pressed = OS.window_fullscreen
	
	# Pas de bouton quitter en version web
	if OS.get_name() == "HTML5":
		$Choix/Quitter.hide()

###################
# *
###################
func animation_clic(sig):
	$Clic.play()
	$Clic.connect("finished",self,"emit_signal",[sig],CONNECT_ONESHOT)
	$Tween.interpolate_property(self, "modulate:a",
		1.0, 0.0, 2,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

###################
# *
###################
func _on_NouvellePartie_pressed():
	animation_clic("nouvellepartie")

###################
# *
###################
func _on_Continuer_pressed():
	animation_clic("continuer")
	
###################
# *
###################
func _on_Quitter_pressed():
	$Clic.play()
	get_tree().quit()

###################
# *
###################
func _on_NouvellePartie_mouse_entered():
	$Choix/NouvellePartie.grab_focus()

###################
# *
###################
func _on_Continuer_mouse_entered():
	$Choix/Continuer.grab_focus()

###################
# *
###################
func _on_Quitter_mouse_entered():
	$Choix/Quitter.grab_focus()

###################
# *
###################
func _on_NouvellePartie_focus_entered():
	$Survol.play()
	
###################
# *
###################
func _on_Continuer_focus_entered():
	$Survol.play()

###################
# *
###################
func _on_Quitter_focus_entered():
	$Survol.play()

###################
# *
###################
func _on_PleinEcran_pressed():
	OS.window_fullscreen = $PleinEcran.pressed

###################
# *
###################
func _on_Langue_pressed():
	if langue == "fr":
		langue = "en"
		$Langue.text = "Français"
	else:
		langue = "fr"
		$Langue.text = "English"
	TranslationServer.set_locale(langue)
	$Choix/Continuer.text = tr("ECRANTITRE_CONTINUER") + " - " + str(temps_de_jeu_humain)
