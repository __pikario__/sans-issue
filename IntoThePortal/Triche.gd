extends Control

signal charger

###################
# *
###################
func _ready():
	self.hide()

###################
# *
###################
func construire(niveaux):
	for niveau in niveaux['acte1']:
		var b = Button.new()
		b.text = niveau
		b.connect("pressed",self,"appui",[{'salle':niveaux['acte1'][niveau],'acte':1.0}])
		$Acte1.add_child(b)
	for niveau in niveaux['acte2']:
		var b = Button.new()
		b.text = niveau
		b.connect("pressed",self,"appui",[{'salle':niveaux['acte2'][niveau],'acte':2.0}])
		$Acte2.add_child(b)
		
	$Acte3.connect("pressed",self,"appui",[{'salle':0,'acte':3.0}])
	
###################
# *
###################
func appui(statut):
	emit_signal("charger",statut)

###################
# *
###################
func _process(_delta):
	if Input.is_action_pressed("triche"):
		if Input.is_action_pressed("espace"):
			self.show()
	else:
		self.hide()
