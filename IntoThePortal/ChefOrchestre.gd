extends Node2D

var ECRAN_TITRE = load("res://EcranTitre.tscn")
var ecran_titre

const FICHIER_SAUVEGARDE = "user://avancement.save"
const FICHIER_TEMPS_DE_JEU = "user://temps_de_jeu.save"

onready var souris = get_node("/root/Souris")
onready var tuto = get_node("/root/Tuto")

const VOLUME_MIN = -60
var lecteurs_en_cours = []

var en_jeu = false
var temps_de_jeu = 0

# intro
var intro

# acte 1
var DAMIER = load("res://generiques/Damier.tscn")
var INTERMEDE = load("res://generiques/Intermede.tscn")
var DEPART = load("res://scenes/acte1/Depart.tscn")

var depart
var damier
var intermede
var transgression = false

var index = -1
var damiers = [
	{'musique':'1','intitule':"TEST0-0",'messages':["TEST0-1","TEST0-2"],'max_trampolines':0,'boutons':[Vector2(1,4)],'receptions':[Vector2(1,2)],'interrupteurs':[Vector2(4,2)],'sonnettes':[],'sonnables':[],'vides':{}},
	{'musique':'1','intitule':"TEST1-0",'messages':["TEST1-1","TEST1-2","TEST1-3"],'max_trampolines':1,'boutons':[Vector2(1,4)],'receptions':[Vector2(1,1)],'interrupteurs':[Vector2(4,1)],'sonnettes':[],'sonnables':[],'vides':{3:[0,1,2,3,4,5]}},
	{'musique':'1','intitule':"TEST2-0",'messages':["TEST2-1","TEST2-2","TEST2-3"],'max_trampolines':1,'boutons':[Vector2(0,5)],'receptions':[Vector2(3,2)],'interrupteurs':[Vector2(1,2)],'sonnettes':[],'sonnables':[],'vides':{3:[0,1,2,3,4,5],1:[0,1,2,3,4,5]}},
	{'musique':'1','intitule':"TEST3-0",'messages':["TEST3-1","TEST3-2","TEST3-3"],'max_trampolines':1,'boutons':[Vector2(5,2)],'receptions':[Vector2(4,2)],'interrupteurs':[Vector2(1,2)],'sonnettes':[],'sonnables':[],'vides':{1:[0,1,2,3,4,5],4:[0,1,2,3,4,5],2:[2],3:[2]}},
	{'musique':'4','intitule':"TEST4-0",'messages':["TEST4-1","TEST4-2","TEST4-3"],'max_trampolines':1,'boutons':[Vector2(1,1),Vector2(5,5)],'receptions':[Vector2(4,4),Vector2(4,5)],'interrupteurs':[Vector2(4,1)],'sonnettes':[Vector2(1,5)],'sonnables':[[Vector2(0,2),Vector2(1,2),Vector2(2,2),Vector2(3,2),Vector2(4,2),Vector2(5,2),Vector2(0,3),Vector2(1,3),Vector2(2,3),Vector2(3,3),Vector2(4,3),Vector2(5,3)]],'vides':{2:[0,1,2,3,4,5],3:[0,1,2,3,4,5]}},	
	{'musique':'4','intitule':"TEST5-0",'messages':["TEST5-1","TEST5-2"],'max_trampolines':1,'boutons':[Vector2(0,5),Vector2(5,5)],'receptions':[Vector2(0,0),Vector2(5,3)],'interrupteurs':[Vector2(5,0)],'sonnettes':[Vector2(0,2)],'sonnables':[[Vector2(3,1),Vector2(4,1),Vector2(5,1),Vector2(3,2),Vector2(4,2),Vector2(5,2),Vector2(2,0)]],'vides':{0:[1,2],1:[0,1,2],2:[1,2],3:[0,1],4:[0]}},
	{'musique':'4','intitule':"TEST6-0",'messages':["TEST6-1","TEST6-2","TEST6-3"],'max_trampolines':2,'boutons':[Vector2(5,5),Vector2(5,0)],'receptions':[Vector2(0,3),Vector2(5,1)],'interrupteurs':[Vector2(0,1)],'sonnettes':[Vector2(0,5)],'sonnables':[[Vector2(3,3),Vector2(5,3)]],'vides':{2:[0,1,2,3,4,5],4:[0,1,2,3,4,5],3:[1,2,3,4,5],5:[1,2],0:[1,2],1:[1,2]}},
	{'musique':'4','intitule':"TEST7-0",'messages':["TEST7-1"],'max_trampolines':2,'boutons':[Vector2(5,0)],'receptions':[Vector2(5,1),Vector2(0,5),Vector2(3,0)],'interrupteurs':[Vector2(0,0)],'sonnettes':[Vector2(4,5),Vector2(5,5),Vector2(2,3)],'sonnables':[[Vector2(0,5),Vector2(0,3)],[Vector2(5,2),Vector2(3,3),Vector2(4,3),Vector2(5,3),Vector2(3,4),Vector2(4,4)],[Vector2(2,0),Vector2(3,0),Vector2(2,1),Vector2(3,1),Vector2(2,2),Vector2(3,2)]],'vides':{3:[0,1,3,4,5],4:[0,1,2,3,4],5:[0,1,2],1:[0,1,2,3],2:[0,1,2,3,5],0:[2,3]}},
	{'musique':'4','intitule':"TEST8-0",'messages':["TEST8-1"],'max_trampolines':2,'boutons':[],'receptions':[Vector2(0,0),Vector2(5,0),Vector2(0,5)],'interrupteurs':[Vector2(0,3)],'sonnettes':[Vector2(1,0),Vector2(4,0),Vector2(5,5),Vector2(0,1)],'sonnables':[[Vector2(1,4),Vector2(2,4),Vector2(3,4),Vector2(4,4),Vector2(5,4)],[Vector2(1,2),Vector2(2,2),Vector2(3,2),Vector2(4,2),Vector2(5,2)],[Vector2(0,0),Vector2(5,0)],[Vector2(0,5)]],'vides':{0:[0,3,5],2:[0,1,2,3,4,5],3:[1,2,3,4,5],4:[0,1,2,4,5],5:[0]}},
	{'musique':'6','intitule':"TEST8-00",'messages':["TEST8-1"],'max_trampolines':2,'boutons':[],'receptions':[Vector2(0,0),Vector2(5,0),Vector2(0,5)],'interrupteurs':[Vector2(0,3)],'sonnettes':[Vector2(1,0),Vector2(4,0),Vector2(5,5),Vector2(0,1)],'sonnables':[[Vector2(1,4),Vector2(2,4),Vector2(3,4),Vector2(4,4),Vector2(5,4)],[Vector2(1,2),Vector2(2,2),Vector2(3,2),Vector2(4,2),Vector2(5,2)],[Vector2(0,0),Vector2(5,0)],[Vector2(0,5)]],'vides':{0:[0,3,5],2:[0,1,2,3,4,5],3:[1,2,3,4,5],4:[0,1,2,4,5],5:[0]}}
	 ]

# acte 2
var SC
var salle_construction

# acte 3
var METRO
var metro

###################
# *
###################
func musique_fondu(lecteur,lire,maxi=-22):
	if lire:
		lecteur.volume_db = VOLUME_MIN
		$Fondu_Musique.interpolate_property(lecteur, "volume_db", VOLUME_MIN, maxi, 1.00, 1, Tween.EASE_IN, 0)
		$Fondu_Musique.start()
		lecteur.play()
		lecteurs_en_cours.append(lecteur)
	else:
		$Fondu_Musique.interpolate_property(lecteur, "volume_db", maxi, VOLUME_MIN, 2.00, 1, Tween.EASE_IN, 0)
		$Fondu_Musique.start()
		$Fondu_Musique.connect("tween_all_completed",lecteur,'stop',[],CONNECT_ONESHOT)
		lecteurs_en_cours.erase(lecteur)

###################
# *
###################
func _ready():
	var fichier_tps = File.new()
	fichier_tps.open(FICHIER_TEMPS_DE_JEU, File.READ)
	temps_de_jeu = int(fichier_tps.get_line())
	
	affichage_ecran_titre()

	var niveaux = {}
	niveaux['acte1'] = {}
	niveaux['acte2'] = {'Salle Construction 3':0,
			'Salle Construction 2':1,
			'Salle Construction 1':2}
	for d in damiers.size():
		niveaux['acte1'][damiers[d].intitule] = d
	niveaux['acte1']['Début'] = -1 
		
	$CanvasNiveau2/Triche.construire(niveaux)
	$CanvasNiveau2/Triche.connect("charger",self,"charger")

	$CanvasNiveau2/Pause.desactiver(true)
	
###################
# *
###################
func _process(delta):
	if en_jeu:
		temps_de_jeu += delta
	
###################
# * pour sauvegarder le temps de jeu lorsque quelqu'un appuie sur la croix
###################	
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		sauver_temps_de_jeu()
		get_tree().quit() # default behavior
	
###################
# *
###################
func affichage_ecran_titre():
	ecran_titre = ECRAN_TITRE.instance()
	ecran_titre.fichier_sauvegarde = FICHIER_SAUVEGARDE
	ecran_titre.temps_de_jeu_humain = temps_de_jeu_humain()
	ecran_titre.connect("nouvellepartie",self,"nouvelle_partie")
	ecran_titre.connect("continuer",self,"charger_sauvegarde")
	if tuto.etat != 0: #ETAT.rien
		tuto.disparaitre()
		tuto.etat = 0
	$CanvasNiveau1.add_child(ecran_titre)

###################
# *
###################	
func afficher_intro():
	$CanvasNiveau1.remove_child(ecran_titre)
	$CanvasNiveau2/Pause.desactiver(false)
	souris.pas_de_curseur()
	
	var INTRO = load("res://scenes/intro/Intro.tscn")
	self.intro = INTRO.instance()
	self.add_child(intro)
	intro.connect("acte1",self,"acte1")
	
###################
# passer d'un épreuve à une autre
###################	
func suivant():
	self.musique_fondu(get_node("Enigme"+damiers[index]['musique']),false)
	if index < self.damiers.size()-1:
		index += 1
	else:
		transgression = true
	
	self.construire_intermede()

###################
# en cas d'échec on recommence la salle
###################
func echec():
	self.construire()

###################
# *
###################
func acte1(i = -1):
	arreter_toute_musique()

	index = i
	
	if self.intro != null:
		self.remove_child(self.intro)
	
	if self.index == -1:
		souris.pas_de_curseur()
		self.depart = DEPART.instance()
		self.add_child(self.depart)
		self.depart.connect("fin",self,"debut")
	else:
		self.construire_intermede()

###################
# *
###################
func debut():
	self.depart.queue_free()
	self.suivant()

###################
# *
###################
func construire():
	self.nettoyer()
	self.musique_fondu($Couloir,false)
	self.musique_fondu(get_node("Enigme"+damiers[index]['musique']),true)
	
	self.damier = DAMIER.instance()
	self.damier.taille_grille = 6
	
	self.damier.intitule = damiers[index]['intitule']
	self.damier.v_boutons = damiers[index]['boutons']
	self.damier.v_receptions = damiers[index]['receptions']
	self.damier.v_interrupteurs = damiers[index]['interrupteurs']
	self.damier.v_vides = damiers[index]['vides']
	self.damier.v_sonnettes = damiers[index]['sonnettes']
	self.damier.v_sonnables = damiers[index]['sonnables']
	self.damier.max_trampolines = damiers[index]['max_trampolines']
	
	self.add_child(self.damier)
	
	self.damier.connect("suivant",self,"suivant")
	self.damier.connect("echec",self,"echec")
	
###################
# *
###################
func construire_intermede():
	# sauvegarde
	sauver(1,self.index)

	self.nettoyer()
	
	self.musique_fondu($Couloir,true,-10)

	# création	
	self.intermede = INTERMEDE.instance()
	self.intermede.trangression = transgression
	self.intermede.connect("fin", self, "construire")
	self.intermede.connect("fin", $Couloir ,"stop")
	self.intermede.connect("tomber", self, "construire_intermede")
	self.intermede.connect("acte2", self, "acte2")
	self.intermede.connect("couper_musique", self, "musique_fondu", [$Couloir,false])
	self.call_deferred("add_child",self.intermede)
	self.intermede.messages_a_afficher(damiers[index]['messages'])
	
	if index > len(damiers)-2:
		self.intermede.afficheur_casse()
		
	souris.pas_de_curseur()	
	
# ACTE 2

###################
# *
###################
func acte2(i = 0):
	self.nettoyer()
	self.arreter_toute_musique()
	
	SC = [load('res://scenes/SalleConstruction3.tscn'),
	load('res://scenes/SalleConstruction2.tscn'),
	load('res://scenes/SalleConstruction1.tscn')]
	index = i

	self.construire_salle_construction()

###################
# *
###################
func construire_salle_construction():
	sauver(2,self.index)
	self.nettoyer()
	
	# musique
	if index < len(SC)-1:
		self.musique_fondu($SalleContructionSuite,true,-12)
	else:
		self.musique_fondu($SalleContructionFinale,true,-12)
	
	self.salle_construction = SC[index].instance()
	self.add_child(self.salle_construction)
	self.salle_construction.connect('salle_suivante',self,'suivant_sc')
	self.salle_construction.connect('echec',self,'construire_salle_construction')

###################
# *
###################		
func suivant_sc():
	
	if self.index < SC.size()-1:
		self.index+=1
		
		# cas dernière salle avec une musique différente
		if self.index == len(SC)-1:
			self.musique_fondu($SalleContructionSuite,false)
		
		self.construire_salle_construction()
		
	else:
		self.musique_fondu($SalleContructionFinale,false)
		acte3()
	
	
# ACTE 3

###################
# *
###################
func acte3():
	self.nettoyer()
	self.arreter_toute_musique()
	
	self.METRO = load('res://scenes/acte3/CouloirsMetro.tscn')
	self.construire_metro()
	souris.pas_de_curseur()
	sauver(3,self.index)

###################
# *
###################
func construire_metro():
	self.metro = self.METRO.instance()
	self.add_child(metro)
	self.metro.connect("sansissue",self,"fin",[],CONNECT_ONESHOT)
	
# FIN

###################
# *
###################
func fin():
	$CanvasNiveau1/Fin.connect("ecran_titre",self,"retour_ecran_titre")
	$CanvasNiveau1/Fin.generique()
	
	$Generique.connect("finished",self,"retour_ecran_titre")
	$Generique.play()
	
	self.remove_child(metro)
	
# AUTRE

###################
# libération des objets
###################
func nettoyer():
	if self.depart != null:
		self.depart.queue_free()
		self.depart = null
	if self.intro != null:
		self.intro.queue_free()
		self.intro = null
	if self.damier != null:
		self.damier.queue_free()
		self.damier = null
	if self.intermede != null:
		self.intermede.queue_free()
		self.intermede = null
	if self.salle_construction != null:
		self.salle_construction.queue_free()
		self.salle_construction = null
	if self.metro != null:
		self.metro.queue_free()
		self.metro = null
		
	 # on essaye de sauvegarder le temps de jeu le plus possible
	sauver_temps_de_jeu()

###################
# *
###################
func arreter_toute_musique():
	for lecteur in lecteurs_en_cours:
		musique_fondu(lecteur, false)

###################
# *
###################
func sauver(acte,salle):
	# affichage du message de sauvegarde
	$CanvasNiveau1/Sauvegarde.show()
	$CanvasNiveau1/Tween.interpolate_property($CanvasNiveau1/Sauvegarde, "modulate:a",
		1.0, 0.0, 2,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$CanvasNiveau1/Tween.start()
	
	# sauvegarde
	var fichier = File.new()
	fichier.open(FICHIER_SAUVEGARDE, File.WRITE)
	var statut = {'acte':acte,'salle':salle}
	fichier.store_line(to_json(statut))
	fichier.close()
	
	sauver_temps_de_jeu()

###################
# *
###################
func sauver_temps_de_jeu():
	var fichier_tps = File.new()
	fichier_tps.open(FICHIER_TEMPS_DE_JEU, File.WRITE)
	fichier_tps.store_line(str(temps_de_jeu))
	fichier_tps.close()

###################
# *
###################
func charger_sauvegarde():
	# lecture depuis le fichier
	var fichier = File.new()
	fichier.open(FICHIER_SAUVEGARDE, File.READ)
	var statut = parse_json(fichier.get_line())
	
	charger(statut)
	en_jeu = true

###################
# *
###################
func charger(statut):
	# nettoyage 
	$CanvasNiveau1.remove_child(ecran_titre)
	$CanvasNiveau2/Pause.desactiver(false)

	# chargement du bon acte
	match statut['acte']:
		1.0:
			acte1(statut['salle'])
		2.0: 
			acte2(statut['salle'])
		3.0:
			acte3()

###################
# *
###################
func nouvelle_partie():
	tuto.reinitialisation()
	afficher_intro()
	temps_de_jeu = 0
	en_jeu = true

###################
# *
###################
func tuto():
	var DEBUT = load("res://scenes/acte1/Depart.tscn")
	var debut = DEBUT.instance()
	add_child(debut)

###################
# *
###################
func retour_ecran_titre():
	musique_fondu($Generique, false)
	
	self.arreter_toute_musique()
	self.nettoyer()
	$CanvasNiveau1/Fin.hide()
	
	sauver_temps_de_jeu()
	affichage_ecran_titre()
	en_jeu = false

###################
# *
###################
func _on_Pause_pause_change_etat(pause):
	en_jeu = not pause

###################
# *
###################
func temps_de_jeu_humain():
	var secondes = fmod(temps_de_jeu,60)
	var minutes = fmod(temps_de_jeu, 3600) / 60
	return "%02d:%02d" % [minutes, secondes]
